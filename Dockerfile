############################################################
# Dockerfile to build CY-2104 from http://www.vfxplatform.com/
# Based on centos6

# build with : sudo docker build -t efesto-cy2014 < Dockerfile
# run with : sudo docker run -i -t efesto-cy2014 /bin/bash

# maintained by
# http://www.efestolab.uk

# Configure build and install:
# gcc 4.1.2 : OK
# python 2.7.3 : OK
# Qt 4.8.5 : OK
# PySide 1.2 : OK
# Boost 1.53: OK
# Intel TBB 4.1: OK
# OpenEXR 2.0.1 : OK
# OpenSubdiv 2.3.3
# Alembic 1.5.x
# OpenColorIO 1.0.7
# Intel TBB 4.1 :  TBD

############################################################

FROM centos:centos6
MAINTAINER Efesto Lab LTD version: 0.1

# set the volume so you can map the built libraries on the current system
VOLUME = ['/opt']

#----------------------------------------------
# SETUP
#----------------------------------------------

# ensure the basic tools are available
RUN yum -y install wget git tar bzip2;

# Download needed packages collected here for rebuild convenience
RUN wget ftp://ftp.gnu.org/gnu/gcc/gcc-4.1.2/gcc-4.1.2.tar.bz2 -P /tmp ;
RUN wget https://www.python.org/ftp/python/2.7.3/Python-2.7.3.tar.bz2 -P /tmp ;
RUN wget http://download.qt.io/archive/qt/4.8/4.8.5/qt-everywhere-opensource-src-4.8.5.tar.gz -P /tmp ;
RUN git clone -b 1.2.2 https://github.com/PySide/pyside-setup.git /tmp/pyside-setup

# Update and install requirements for building
RUN yum -y install \
    gcc-c++.x86_64 \
    glibc-devel.x86_64 \
    glibc-devel.i686 \
    zlib-devel.x86_64 \
    texinfo.x86_64 \
    libXext-devel;


#----------------------------------------------
# build and install GCC412 in /opt/gcc412
#----------------------------------------------

RUN cd /tmp && \
    tar -jxvf /tmp/gcc-4.1.2.tar.bz2 && \
    cd gcc-4.1.2 && \
    ./configure \
        --prefix=/opt/gcc412 \
        --program-suffix=412 \
        --enable-shared \
        --enable-threads=posix \
        --enable-checking=release \
        --with-system-zlib \
        --disable-libunwind-exceptions \
        --enable-languages=c,c++ \
        --enable-__cxa_atexit && \
    make -j 4 && \
    make install && \
    cd /usr/bin && \
    ln -s /opt/gcc412/bin/gcc412 . && \
    ln -s /opt/gcc412/bin/g++412 . ;

# set the version of gcc 412 as default compiler from now on.
ENV CC gcc412
ENV CXX g++412


#----------------------------------------------
# build and install PYTHON 273 /opt/python273
#----------------------------------------------

RUN cd /tmp && \
    tar -jxvf /tmp/Python-2.7.3.tar.bz2 && \
    cd Python-2.7.3 && \
    ./configure \
         --prefix=/opt/python273 \
         --program-suffix=273 \
         --enable-unicode ucs4 \
         --enable-shared \
         --enable-ipv6 yes && \
    make -j 4 && \
    make install &&\
    cd /usr/bin && \
    ln -s /opt/python273/bin/python python2.7;

# add the new path to the standard library path
RUN echo '/opt/python273/lib' > /etc/ld.so.conf.d/python-273.conf && ldconfig;

#----------------------------------------------
# build and install QT 485 /opt/qt485
#----------------------------------------------

# install some dependencies for Qt
RUN yum -y install \
    openssl-devel \
    libXext-devel \
    libXt-devel \
    qt4-devel \
    qtwebkit-devel \
    qt-devel;


RUN cd /tmp && \
   tar -zxvf qt-everywhere-opensource-src-4.8.5.tar.gz && \
   cd qt-everywhere-opensource-src-4.8.5 && \
   ./configure \
        -prefix /opt/qt4   \
        -confirm-license   \
        -opensource        \
        -openssl           \
        -release           \
        -no-phonon         \
        -no-phonon-backend \
        -no-nis            \
        -no-openvg         \
        -no-ssse3 \
        -no-webkit \
        -nomake demos      \
        -no-declarative \
        -nomake examples \
        -v &&\
   make -j 4 && \
   make install;

RUN echo '/opt/qt4/lib' > /etc/ld.so.conf.d/qt-485.conf && ldconfig;
# fix path for qmake ?
ENV PATH /opt/qt4/bin/:$PATH

#----------------------------------------------
# build and install PYSIDE 122 /opt/python273
#----------------------------------------------
RUN yum -y install cmake

RUN wget http://download.qt-project.org/official_releases/pyside/pyside-qt4.8+1.2.2.tar.bz2 -P /tmp &&\
    wget http://download.qt-project.org/official_releases/pyside/shiboken-1.2.2.tar.bz2 -P /tmp &&\
    wget https://github.com/PySide/Tools/archive/0.2.15.tar.gz -P /tmp;

RUN cd /tmp &&\
    tar -jxvf /tmp/pyside-qt4.8+1.2.2.tar.bz2 &&\
    tar -jxvf /tmp/shiboken-1.2.2.tar.bz2  &&\
    tar -zxvf /tmp/0.2.15.tar.gz &&\
    cd /tmp/shiboken-1.2.2 &&\
    mkdir build &&\
    cd build &&\
    cmake \
        -DPYTHON_INCLUDE_DIR=/opt/python273/include/python2.7 \
        -DPYTHON_LIBRARY=/opt/python273/lib/libpython2.7.so.1.0 \
        -DCMAKE_INSTALL_PREFIX=/opt/python273 \
        .. &&\
    make -j 4 &&\
    make install &&\
    cd /tmp/pyside-qt4.8+1.2.2 &&\
    mkdir build &&\
    cd build &&\
    cmake \
        -DCMAKE_INSTALL_PREFIX=/opt/python273 \
        .. &&\
    make -j 4 &&\
    make install &&\
    cd /tmp/Tools-0.2.15 &&\
    mkdir build &&\
    cd build &&\
    cmake \
        -DCMAKE_INSTALL_PREFIX=/opt/python273 \
        .. &&\
    ldconfig;

# #----------------------------------------------
# # BOOST 1.53 /opt/boost_1_53_0_gcc412
# #----------------------------------------------
RUN yum -y install bzip2-devel libicu-devel
RUN wget http://downloads.sourceforge.net/project/boost/boost/1.53.0/boost_1_53_0.tar.bz2 -P /tmp
RUN cd /tmp &&\
    tar -jxvf /tmp/boost_1_53_0.tar.bz2 &&\
    cd /tmp/boost_1_53_0 &&\
    echo -e 'using gcc : : /opt/gcc412/bin/g++412 ;' > tools/build/v2/user-config.jam &&\
    echo -e '\nusing qt : /opt/qt4 ;' >> tools/build/v2/user-config.jam &&\
    ./bootstrap.sh \
         --with-python=python2.7 \
         --with-toolset=gcc \
         --prefix=/opt/boost_1_53_0_gcc412 &&\
    ./b2 -d2 &&\
    ./b2 install;

# #----------------------------------------------
# # TBB 4.1
# #----------------------------------------------
RUN wget  https://www.threadingbuildingblocks.org/sites/default/files/software_releases/source/tbb41_20130613oss_src.tgz -P /tmp;
RUN cd /tmp &&\
    tar -zxvf /tmp/tbb41_20130613oss_src.tgz &&\
    cd tbb41_20130613oss &&\
    cp build/linux.gcc.inc build/linux.gcc412.inc &&\
    sed -i 's/gcc/gcc412/g' build/linux.gcc412.inc &&\
    sed -i 's/g++/g++412/g' build/linux.gcc412.inc &&\
    make compiler=gcc412 -j 4 &&\
    mkdir -p /opt/tbb41/include &&\
    mkdir -p /opt/tbb41/serial &&\
    mkdir -p /opt/tbb41/lib &&\
    cp -r include/serial/* /opt/tbb41/serial/ &&\
    cp -r include/serial/* /opt/tbb41/serial/ &&\
    cp build/linux_intel64_gcc412_cc4.4.7_libc2.12_kernel3.15.9_release/*.so* /opt/tbb41/lib &&\
    echo '/opt/tbb41/lib' > /etc/ld.so.conf.d/tbb-41.conf &&\
    ldconfig;


# #----------------------------------------------
# # OpenEXR 2.0.1 : /opt/openexr201/
# #----------------------------------------------

RUN wget http://download.savannah.nongnu.org/releases/openexr/pyilmbase-2.0.1.tar.gz -P /tmp &&\
    wget http://download.savannah.nongnu.org/releases/openexr/openexr-2.0.1.tar.gz -P /tmp &&\
    wget http://download.savannah.nongnu.org/releases/openexr/ilmbase-2.0.1.tar.gz -P /tmp;

RUN cd /tmp &&\
    tar -zxvf /tmp/ilmbase-2.0.1.tar.gz &&\
    cd /tmp/ilmbase-2.0.1 &&\
    ./configure --prefix=/opt/openexr201/ &&\
    make -j 4  &&\
    make install &&\
    echo '/opt/openexr201/lib' > /etc/ld.so.conf.d/openexr-201.conf &&\
    ldconfig &&\
    cd /tmp &&\
    tar -zxvf /tmp/openexr-2.0.1.tar.gz &&\
    cd /tmp/openexr-2.0.1 &&\
    ./configure \
        --prefix=/opt/openexr201/ \
        --with-ilmbase-prefix=/opt/openexr201/ &&\
    make -j 4  &&\
    make install &&\
    ldconfig;
